package main.java.userinput;

public class ANSI {
  static final String ESC = "\033";
  public static String escape(int val){
    return ESC+"["+val+";40"+"m"; //;40: "and black background"
    //ie red ansi code: "\033[31m"
  }

  public static String cursorRight(int n){
    return ESC+"["+n+"C";
  }
  public static String cursorLeft(int n){
    return ESC+"["+n+"D";
  }

  //Private. Use red(), green() etc.
  private static String color(String text, String colorCode){
    return colorCode + text + CLEAR;
  }

  //Removes ansi escape codes from text. Use before printouts in places not supporting color.
  public static String decolor(String coloredText){
    final String regex = ESC+"\\[[;\\d]*m";
    return coloredText.replaceAll(regex, "");
  }

  public final static String CLEAR = escape(0);
  public final static String BLACK = escape(30);
  public final static String RED = escape(31);
  public final static String GREEN = escape(32);
  public final static String YELLOW = escape(33);
  public final static String BLUE = escape(34);
  public final static String MAGENTA = escape(35);
  public final static String CYAN = escape(36);
  public final static String WHITE = escape(37);
  
  public final static String BOLD = escape(1);
  public final static String FAINT = escape(2);
  public final static String INVERT = escape(7);

  //Hmm, is there any smarter way to dynamically define these in Java?
  public final static String black(String text){return color(text,BLACK);}
  public final static String red(String text){return color(text,RED);}
  public final static String green(String text){return color(text,GREEN);}
  public final static String yellow(String text){return color(text,YELLOW);}
  public final static String blue(String text){return color(text,BLUE);}
  public final static String magenta(String text){return color(text,MAGENTA);}
  public final static String cyan(String text){return color(text,CYAN);}
  public final static String white(String text){return color(text,WHITE);}

  public final static String bold(String text){return color(text,BOLD);}
  public final static String faint(String text){return color(text,FAINT);}
  public final static String invert(String text){return color(text,INVERT);}
}
