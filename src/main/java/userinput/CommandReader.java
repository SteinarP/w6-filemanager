package main.java.userinput;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import main.java.filemanipulation.FileService;
import main.java.logging.FileLogger;

public class CommandReader {
  String rawString;
  Map<String, Runnable> commands;
  Map<String, String> descriptions;
  Scanner scanner;
  static private final String DESC_START = "#";

  FileService fs;
 
  public CommandReader(){
    scanner = new Scanner(System.in);
    commands = new HashMap<String, Runnable>();
    descriptions = new HashMap<String, String>();
    descriptions.put("help", "Prints all available commands");
    descriptions.put("exit", "Exit the program");
    descriptions.put("dir <dirPath> [extension]", "Lists content of targeted directory. If extension provided, only matches this type of file.");
    descriptions.put("file <filePath> [searchWord]", "Shows info about targeted file. If searchWord provided and file is text, it will also count how many times this word occurs.");
    descriptions.put("test", "Tests multiple commands to show that they work.");
    fs = new FileService();
  }
  
  /*
  public void addCommand(String alias, String desc, Runnable cmd){
    commands.put(alias, cmd);
    descriptions.put(alias, desc);
  }*/

  public void exitSafely(){
    scanner.close();
    FileLogger fl = new FileLogger();
    fl.writeToLog("Exiting.");
    System.out.println("Goodbye!");
    System.exit(0);
  }

  public void printCommands(){
    for (String alias : descriptions.keySet()){
      String desc = descriptions.get(alias);
      System.out.println(" "+ANSI.yellow(alias) + ": " + desc);
    }
  }
   
  public void startReading(){ 
    System.out.println("System ready. Use command "+ANSI.blue("help")+" to list commands."); 
    while (true){
      String input = getInput("Enter command");
      handleCommandInput(input);
    }
  }



  public void handleCommandInput(String input){
    String[] args = input.split(" ");
    if (args == null || args.length == 0){
      System.out.println("No input provided!");
      return;
    }
    String alias = args[0];
    
    switch (alias){
      case "exit": case "quit": exitSafely(); break;
      case "help": printCommands(); break;
      case "dir": {
        if (args.length == 1){
          fs.getFileNames(); return;
        } else
        if (args.length == 2){
          fs.getFileNames(args[1]); return; //dirPath
        } else
        if (args.length >= 3){
          fs.getFileNames(args[1], args[2]); return; //dirPath, extension
        } 
        if (args.length > 3){
          System.out.println("Ignored redundant arguments: "+args[3]+"...");
        }
      } break;
      case "file": {
        if (args.length <= 1){
          System.out.println("Missing filePath argument.");
          return;
        } else
        if (args.length == 2){
          fs.getFileDetails(args[1]);
        } else if (args.length == 3){
          fs.getFileDetails(args[1], args[2]);
        }
      } break;
      case "test": {
        handleCommandInput("this-is-invalid");
        handleCommandInput("dir");
        handleCommandInput("dir ./src/nonexistant");
        handleCommandInput("dir ./src/main/java/resources jpg");
        handleCommandInput("file ./src/main/java/resources/Meme_3.jpg this_argument_does_nothing_on_images");
        handleCommandInput("file ./src/main/java/resources/Dracula.txt vampire");
        handleCommandInput("file ./src/main/java/resources/Dracula.txt night");
      } break;
      default: System.out.println("\""+ANSI.red(input)+"\" is not a valid command!"); break;
    }

  }

  public String getInput(String description){
    System.out.println(ANSI.blue(description));
    System.out.print(">");
    
    String textInput = "";

    try{
      textInput = scanner.nextLine();
    } catch (Exception e){
      System.err.println(e.getMessage());
    }
    //scanner.close();
    System.out.println(ANSI.CLEAR);
    return textInput;
  }
}
