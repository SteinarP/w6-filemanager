package main.java.logging;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import main.java.userinput.ANSI;

/**
 * Logging module Write method output strings to a log file Log all of
 * filemanipulation package's action Log time and duration of each function call
 */

public class FileLogger {

  String logFilePath;
  File logFile;
  FileWriter fwriter;
  Date start;
  long startMillis;

  public FileLogger(){
    logFilePath = "./log/methodlog";
    try{
      logFile = new File(logFilePath);
      fwriter = new FileWriter("./log/FileService.txt");
    } catch (IOException e){
      System.err.println("Error making new file");
      e.printStackTrace();
    } finally {
      //System.out.println("Logfile setup complete.");
    }
  } 

  public void writeToLog(String content){
    DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
    String stamp = (start==null) ? "[???]" : "["+dateFormat.format(new Date())+"]";
    String logLine = stamp + " - " + ANSI.decolor(content) +"\n";
    try {
      fwriter.write(logLine+"\n");
      fwriter.flush();
      //fwriter.close();
    } catch (IOException e){
      System.out.println("Error writing log");
      e.printStackTrace();
    } finally {
      //Finished write.
    }
  }
  
  public void startLogTimer(){
    this.start = new Date();
    this.startMillis = System.currentTimeMillis();
  }

  public void clearLogTimer(){
    this.start = null;
    this.startMillis = 0;
  }

  
  public void timedWrite(String content){
    timedWrite(content, true);
  }
  public void timedWrite(String content, boolean alsoPrint){
    if (this.start == null || this.startMillis == 0){
      System.err.println("Failed to log method duration. Missing a preceding startLogTimer() call!");
    } else {
      long ms = System.currentTimeMillis() - startMillis;  
      content += ANSI.invert("("+ms+" ms)");
    }

    if (alsoPrint){
      System.out.println(content);
    }
 
    writeToLog(content);
    this.clearLogTimer();
  }
}
