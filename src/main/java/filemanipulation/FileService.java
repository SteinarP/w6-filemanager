package main.java.filemanipulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import main.java.logging.FileLogger;
import main.java.userinput.ANSI;

public class FileService {
  FileLogger flog;

  public FileService(){
    flog = new FileLogger();
  }

  public void getFileNamesHere(){
    getFileNames("./");
  }

  public void getFileNames(String dirPath, String extension){
    flog.startLogTimer();
    File directory = new File(dirPath);
    File[] filesHere = directory.listFiles();
    String output = "";

    if (filesHere == null){
      flog.timedWrite(ANSI.red("Failed to find files at path \""+dirPath+"\"!"));
      return;
    }

    ArrayList<File> realFiles = new ArrayList<File>();
    ArrayList<File> actuallyDirs = new ArrayList<File>();
    for (File f : filesHere){
      if (f.isFile()) realFiles.add(f);
      else if (f.isDirectory()) actuallyDirs.add(f);
    }
    
    String status = extension == null ? "" : ", only \""+ANSI.green(extension)+"\"";
    output += "Directory \""+ANSI.yellow(dirPath)+"\""+status+":\n";
    if (extension == null){
      for (File d : actuallyDirs){
        output += "/"+ ANSI.yellow(d.getName())+"\n";
      }
    }

    for (File f : realFiles){
      if (extension == null || f.getName().endsWith(extension)){
        output += " "+ANSI.green(f.getName())+"\n";
      }
    }

    flog.timedWrite(output);
  }

  public void getFileNames(String dirPath){
    getFileNames(dirPath, null);
  }

  public void getFileNames(){
    System.out.println("No filepath provided. Defaulting to root \"./\".");
    getFileNames("./", null);
  }


  public String byteSizeString(long bytes){
    return byteSizeString(bytes, false);
  }
  public String byteSizeString(long bytes, boolean fullUnitName){
    String[] prefix = {" ", "kilo", "mega", "giga", "tera", "peta", "exa"};
    int i = 0;
    for (i=0; i<prefix.length && bytes > 1000; i++){
      bytes /= 1024; //2^10
    }
    String unit = fullUnitName ? prefix[i].trim() : (prefix[i].charAt(0)+"b").trim().toUpperCase(); //kilobytes --> KB
    return bytes+" "+unit;
  };

  public void getFileDetails(String filePath){
    getFileDetails(filePath, null);
  }
  public void getFileDetails(String filePath, String searchWord){
    flog.startLogTimer();
    
    File file = new File(filePath);
    if (file == null || !file.exists()){
      flog.timedWrite(ANSI.red("Failed to find file at \""+filePath+"\"!\n"));
      return;
    }

    String output = "File \""+ANSI.green(filePath)+"\"\n";
    //Name, size
    String fileName = file.getName();
    String byteSize = byteSizeString(file.length());
    output += fileName + " - " + byteSize + "\n";
    
    //Done if non-text file
    Set<String> supported = Set.of("txt", "md", "java");
    String extension = fileName.substring(fileName.lastIndexOf(".")+1); //Warning: Assumes no file with name "DotAtEnd."
    if (false == supported.contains(extension)){
      flog.timedWrite(output);
      return;
    }
    
    if (searchWord == null || searchWord.trim().length() == 0){
      System.out.println("Tip: You can provide a search word to count occurances in the file.");
      flog.timedWrite(output);
      return;
    } else { 
      //We have a supported file format for text search.
      BufferedReader br;
      int wordCount = 0;
      try {
        br = new BufferedReader(new FileReader(file), 1024*20);
        String line; int lnum=0; 
        searchWord = searchWord.toLowerCase();
        String regex = "[\\W+ ]"; //"[\\W+\s]"; //Non-alphanumericals, whitespace
        while ((line = br.readLine()) != null){
          lnum++;
          for (String word : line.toLowerCase().split(regex)) {
            wordCount += word.equals(searchWord) ? 1 : 0;
          }
        }
        output += ("Lines: "+lnum +"\n");
      } catch (IOException e){
        System.err.println(e.getMessage());
      } 
  
      output += "Word \""+searchWord+"\" was found "+wordCount+" times.\n";
      flog.timedWrite(output);
    }


  }
}
