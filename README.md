# File Management System
## Java Terminal Program

```
  javac -d .\out .\src\main\java\filemanipulation\*.java .\src\main\java\logging\*.java .\src\main\java\userinput\*.java .\src\main\java\*.java
```

Assuming you're in root directory FileManagerSystem: 
```
  cd .\out\main\java\
```
Either of these should work in theory
```
  java Program
  java main.java.Program
```
If not, (Error: Could not find or load main class Program), run from IDE.

src\main\java\resources is the default location for files to test on.
Use command *help* to list available commands.
Commands using files are logged to .\log\FileService.txt (overwritten on next launch)

See ./screenshots for examples of program working.